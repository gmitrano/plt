package plt;

public class translator {
	
	public static final String NIL = "nil";
	
	private String Phrase = "";
	
	public translator (String inputPhrase) {
		
		Phrase = inputPhrase;
		
	}
	
	public Object getPhrase() {
		return Phrase;
	}
	
	public String translate() {
		if(startWithVowell()) {
			if(Phrase.endsWith("y")) {
				return Phrase + "nay";
			}else if (endWithVowell()) {
				return Phrase + "yay";
			}
			
		}
		
		return NIL;
	}
	
	private boolean startWithVowell( ) {
		return Phrase.startsWith("a") || Phrase.startsWith("e") || Phrase.startsWith("i") ||Phrase.startsWith("o") ||Phrase.startsWith("u");
		
	}
	
	private boolean endWithVowell( ) {
		return Phrase.startsWith("a") || Phrase.startsWith("e") || Phrase.startsWith("i") ||Phrase.startsWith("o") ||Phrase.startsWith("u");
		
	}
	
}
